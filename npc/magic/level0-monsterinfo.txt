// The Mana World script
// Author: Jesusalva <jesusalva@themanaworld.org>
//
// Magic Script: SKILL_MONSTERINFO (Level 1)
// School: General 1

function	script	SK_Miteyo	{
    .@mobId=getunitdata(@skillTarget, UDT_CLASS);
    if (.@mobId > 1000) {
        // Decide how much detail you get based on how much magic you have
        if (getskilllv(SKILL_MAGIC) > 2) {
            charcommand("@mi "+.@mobId);
        } else {
            .@mhp = getmonsterinfo(.@mobId, MOB_MAXHP);
            // Truncate HP
            if (.@mhp > 1000)
                .@mhp -= .@mhp % 1000;
            else if (.@mhp > 100)
                .@mhp -= .@mhp % 100;
            else
                .@mhp -= .@mhp % 10;
            // Randomize atk
            .@atk = rand2(getmonsterinfo(.@mobId, MOB_ATK1),
                          getmonsterinfo(.@mobId, MOB_ATK2));
            // Send the obfuscated information
            dispbottom l("%s - Level %d (~%s HP, ~%s ATK)",
                         getmonsterinfo(.@mobId, MOB_NAME),
                         getmonsterinfo(.@mobId, MOB_LV),
                         fnum(.@mhp),
                         fnum(.@atk));
        }
    } else {
        // Not a valid monster
        dispbottom l("This is not a monster; I cannot use monsterinfo on it.");
    }
    return;
}

