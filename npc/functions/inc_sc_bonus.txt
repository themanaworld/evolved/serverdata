// TMW-2 Script.
// Author:
//    Jesusalva
// Description:
//    Applies effects for INC_* (STR doesn't exist)
//    Valid values: INCAGI INCVIT INCINT INCDEX INCLUK INCHIT INCFLEE SC_FURY
//    Doesn't works: SC_STRUP
//    Works if .@min == .@max: INCMHP INCMHPRATE INCMSP INCMSPRATE
///   Untested Values: WALKSPEED (reverse logic) INVINCIBLE (broken)
//    PS. SC_FURY causes crit rate to increase
//
// Variables:
//    .@delay    Second of buffing
//    .@type     SC_*
//    .@min      Min amount of type
//    .@max      Max amount of type (optional)

// SC_Bonus(delay, SC, min{, max})
function	script	SC_Bonus	{
    .@delay=getarg(0);
    .@type=getarg(1);
    .@min=getarg(2);
    .@max=getarg(3, .@min);
    if (.@delay <= 0)
        return false;

    // Get the bonus value
    if (.@min != .@max)
        .@bonus=rand2(.@min, .@max);
    else
        .@bonus=.@min;

    // Remaining time and effect conversion
    .@v=getstatus(.@type, 1);
    .@t=getstatus(.@type, 5);

    // Convert remaining time to seconds, rounded down
    if (.@t > 1000)
        .@t=.@t/1000;
    else
        .@t=0;

    // If there was effect previously, get ponderate average
    if (.@v > 0)
        .@v=ponderate_avg(.@bonus, .@delay, .@v, .@t);
    else
        .@v=.@bonus;

    // Update time value to ms and to stack
    .@t+=.@delay;
    .@t*=1000;

    // Debug print if needed
    if (debug || $@GM_OVERRIDE)
        debugmes "Effect %d (+%d percent) for %d ms", .@type, .@bonus, .@t;

    // Restart the bonus
    sc_end .@type;
	sc_start .@type,.@t,.@v;
    return true;
}

// SC_Bonus2(delay, SC1, val1, val2)
function	script	SC_Bonus2	{
    .@delay=getarg(0);
    .@type=getarg(1);
    .@val1=getarg(2);
    .@val2=getarg(3);
    if (.@delay <= 0)
        return false;

    // Remaining time and effect conversion
    .@v1=getstatus(.@type, 1);
    .@v2=getstatus(.@type, 2);
    .@ts=getstatus(.@type, 5);

    // Convert remaining time to seconds, rounded down
    .@ts=.@ts/1000;

    // If there was effect previously, get ponderate average (val1)
    if (.@v1 > 0)
        .@v1=ponderate_avg(.@val1, .@delay, .@v1, .@ts);
    else
        .@v1=.@val1;

    // If there was effect previously, get ponderate average (val2)
    if (.@v2 > 0)
        .@v2=ponderate_avg(.@val2, .@delay, .@v2, .@ts);
    else
        .@v2=.@val2;

    // Update time value to ms and to stack
    .@t+=.@delay;
    .@t*=1000;

    // Debug print if needed
    if (debug || $@GM_OVERRIDE)
        debugmes "Effect %d (%d/%d bonus) for %d ms", .@type, .@v1, .@v2, .@ts;

    // Restart the bonus
    sc_end .@type;
	sc_start2 .@type, .@ts, .@v1, .@v2;
    return true;
}

