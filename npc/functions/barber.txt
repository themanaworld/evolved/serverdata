// Evol scripts.
// Authors:
//    omatt
//    Reid
//    Travolta
//    Jesusalva
// Description:
//    Function for supporting barber NPC.

// BarberSayStyle({what})
// what: 1 = Style; 2 = Color; 3 = Style + Color in dialog
function	script	BarberSayStyle	{

    .@get_color = getlook(LOOK_HAIR_COLOR);
    .@get_look = getlook(LOOK_HAIR);
    .@style_name$ = $@hairstyle$[.@get_look];
    .@color_name$ = $@haircolor$[.@get_color];

    switch (getarg(0, 3))
    {
        case 1:
            message strcharinfo(0), l("%s", .@style_name$);
            break;
        case 2:
            message strcharinfo(0), l("%s", .@color_name$);
            break;
        case 3:
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("Your hairstyle is @@ and its color is @@.", .@style_name$, .@color_name$);
            break;
    }
    return;
}
function	script	BarberChangeStyle	{
    do
    {
        .@hairsizearray = getarraysize($@hairstyle$);
        .@get_look = getlook(LOOK_HAIR);

        // Here .@i starts from 1 because hairstyle 0 doesn't exist.
        for (.@i = 1; .@i < .@hairsizearray; .@i++)
        {
            .@menustr$ = .@menustr$
                        + rif(.@get_look != .@i, l("" + $@hairstyle$[.@i] + ""))
                        + ":";
        }

        .@menustr$ = .@menustr$ + l("I'm fine for now, thank you.");

        .@idx = select(l("As you want!"),.@menustr$);

        if (.@idx == .@i + 1) return; // last choice to quit dialog

        switch (@menu)
        {
            case 1:
                do
                {
                    // here "- 1" because i don't use the 0 of array
                    .@rand_hair = rand(1,(.@hairsizearray - 1));
                } while (.@rand_hair == getlook(LOOK_HAIR));
                setlook LOOK_HAIR, .@rand_hair;
                setlook LOOK_HAIR_COLOR, getlook(LOOK_HAIR_COLOR);
                BarberSayStyle(1);
                break;
            default:
                // and here "- 1" because the first choice is taken by the random
                setlook LOOK_HAIR, (@menu - 1);
                setlook LOOK_HAIR_COLOR, getlook(LOOK_HAIR_COLOR);
                break;
        }
        .@menustr$ = "";
    } while (1);
}

function	script	BarberChangeColor	{
    do
    {
        .@get_look = getlook(LOOK_HAIR_COLOR);
        .@hairsizearray = getarraysize($@haircolor$);

        for (.@i = 0; .@i < .@hairsizearray; .@i++)
        {
            .@menustr$ = .@menustr$
                        + rif(.@get_look != .@i && .@i != HC_PURPLE,
                              l("" + $@haircolor$[.@i] + ""))
                        + ":";
        }

        .@menustr$ = l("Surprise me!") + ":" + .@menustr$ + l("I'm fine for now, thank you.");

        .@idx = select(.@menustr$);

        if (.@idx == .@i + 2) return;

        switch (@menu)
        {
            case 1:
                do
                {
                    .@rand_color = rand(0, .@hairsizearray);
                } while (.@rand_color == getlook(LOOK_HAIR_COLOR));
                setlook LOOK_HAIR_COLOR, .@rand_color;
                BarberSayStyle(2);
                break;
            default:
                setlook LOOK_HAIR_COLOR, (@menu - 2);
                break;
        }
        .@menustr$ = "";
    } while (1);

    return;
}

// 3 = Abs (Real value is 1)
// 2 = Boobs (Real value is 0)
// 1 = Shirt (Real value is 3)
function	script	BarberChangeBodyType	{
    mesn l("Note");
    mes b(l("Changing your body type will send you back to the character selection screen."));
    next();

    mes l("Please select the desired body type:");
    menuint(
        rif(BodyType == BODYTYPE_3, "► ") + l("Body type %s", "A"), BODYTYPE_3,
        rif(BodyType == BODYTYPE_2, "► ") + l("Body type %s", "B"), BODYTYPE_2,
        rif(BodyType == BODYTYPE_1, "► ") + l("Body type %s", "C"), BODYTYPE_1);

    if (BodyType == @menuret) {
        return; // don't kick to char selection when not needed
    }

    // FIXME: when manaplus supports seamless changing for evol2, use a simple return;
    closedialog();
    close2();
    BodyType = @menuret;
    close;
}

function	script	BarberChangeRace	{

    mes l("What's your race?");
    mesc l("WARNING: Changing race may have side effects.");
    menuint
        l("Talpan"), Talpan,
        l("Tritan"), Tritan,
        l("Ifriton"), Ifriton,
        rif(is_dev(), l("Gispaan")), Gispaan,
        rif(is_dev(), l("Sparron")), Sparron,
        rif(countitem(SkeletonCharm), l("Undead")), Skellie;

    mes "";

    // Not needed
    if (Class == @menuret)
        return;

    // Change race and we're done
    //Class = @menuret;
    jobchange(@menuret); // STUPID idea, but imposed by Hercules
    return;
}

// Jack of all trades
// Barber({intro=True})
function	script	Barber	{
    if (getarg(0, true)) {
        mesn;
        mesq l("Hello.");
        next;
    }
    mesq l("What would you like me to do?");
    next;
    do
    {
        select
            l("What is my current hairstyle and hair color?"),
            l("I'd like to get a different style."),
            l("Can you do something with my color?"),
            l("How about changing my body type?"),
            rif(BaseLevel > 70, l("I would like to change my species.")),
            l("I'm fine for now, thank you.");

        switch (@menu)
        {
        case 1:
            BarberSayStyle();
            break;
        case 2:
            BarberChangeStyle();
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("Enjoy your new style."),
                l("Anything else?");
            break;
        case 3:
            BarberChangeColor();
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("I hope you like this color."),
                l("Anything else?");
            break;
        case 4:
            BarberChangeBodyType();
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("You look fantastic."),
                l("Anything else?");
            break;
        case 5:
            BarberChangeRace();
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("This service is provided with no warranties on regards of side effects."),
                l("Anything else?");
            break;
            break;
        case 6:
            speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
                l("Feel free to come visit me another time.");
            goodbye;
            return;
        }
    } while (true);
    return;
}

