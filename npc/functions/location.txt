// TMW2 Script
// Author: Jesusalva
// Location Config

-	script	loc_config	32767,{
    end;

OnInit:
    // TP_FORT TP_BOSSR
    setarray $@LOCMASTER_TP,  TP_CANDOR,TP_TULIM,TP_HURNS,TP_NIVAL;
    setarray $@LOCMASTER_LOC$, "Candor", "Tulim", "Hurns", "Nival";
    setarray $@LOCMASTER_MAP$,  "029-1", "001-1", "009-1", "020-1";
    setarray $@LOCMASTER_X,          46,      51,      52,      75;
    setarray $@LOCMASTER_Y,          97,      78,      41,      85;

    //debugmes "Locmaster: Index 0: %s [%s.gat (%d, %d)]", $@LOCMASTER_LOC$[0], $@LOCMASTER_MAP$[0], $@LOCMASTER_X[0], $@LOCMASTER_Y[0];
    //debugmes "Locmaster: Index 2: %s [%s.gat (%d, %d)]", $@LOCMASTER_LOC$[2], $@LOCMASTER_MAP$[2], $@LOCMASTER_X[2], $@LOCMASTER_Y[2];
    //debugmes "Locmaster: Index 5: %s [%s.gat (%d, %d)]", $@LOCMASTER_LOC$[5], $@LOCMASTER_MAP$[5], $@LOCMASTER_X[5], $@LOCMASTER_Y[5];
    end;
}

// Resaves your respawn point
function	script	ResaveRespawn	{
    .@i=array_find($@LOCMASTER_LOC$, LOCATION$);
    savepoint $@LOCMASTER_MAP$[.@i], $@LOCMASTER_X[.@i], $@LOCMASTER_Y[.@i];
    return;
}

// Warps you to last visited town
function	script	ReturnTown	{
    .@i=array_find($@LOCMASTER_LOC$, LOCATION$);
    warp $@LOCMASTER_MAP$[.@i], $@LOCMASTER_X[.@i], $@LOCMASTER_Y[.@i];
    return;
}

// Convert map name to location id
// LocToMap( LocName )
function	script	LocToMap	{
    // Fill variable
    .@v$=getarg(0);

    // Error code
    if (playerattached())
        .@err=RB_DEFAULT;
    else
        .@err=RB_DEBUGMES;

    // Validade variable, see npc/config/location.txt first
    .@lx=array_find($@LOCMASTER_LOC$, .@v$);
    if (.@lx < 0)
        return Exception("Invalid location passed to LocToMap: "+.@v$, .@err);

    return $@LOCMASTER_MAP$[.@lx];
}

// Convert map name to location id
// MapToLoc( MapName )
function	script	MapToLoc	{
    // Fill variable
    .@v$=getarg(0);

    // Error code
    if (playerattached())
        .@err=RB_DEFAULT;
    else
        .@err=RB_DEBUGMES;

    // Validade variable, see npc/config/location.txt first
    .@lx=array_find($@LOCMASTER_MAP$, .@v$);
    if (.@lx < 0)
        return Exception("Invalid map passed to MapToLoc: "+.@v$, .@err);

    return $@LOCMASTER_LOC$[.@lx];
}

// Gets the location code for TP code
function	script	TPToLoc	{
    .@i=array_find($@LOCMASTER_TP, getarg(0));
    return $@LOCMASTER_MAP$[.@i];
    return;
}

// Convert LOC (uppercase) to a TP variable
// POL_LocToTP( {TOWNCODE} )
function	script	POL_LocToTP	{
    .@tw$=strtoupper(getarg(0, LOCATION$));

    // TODO: Change this to use the arrays instead
    if (.@tw$ == "TULIM")
        return TP_TULIM;

    if (.@tw$ == "HURNS")
        return TP_HURNS;

    if (.@tw$ == "NIVAL")
        return TP_NIVAL;

    if (.@tw$ == "CANDOR")
        return TP_CANDOR;

    return Exception("Invalid town requested / POL_LocToTP", RB_DEFAULT|RB_SPEECH, -1);
}

// Upon entering a town
// EnterTown( LocName )
function	script	EnterTown	{
    // Fill variable
    .@v$=getarg(0);

    // Validade variable, see npc/config/location.txt first
    if (array_find($@LOCMASTER_LOC$, .@v$) < 0)
        return Exception("Invalid location passed to EnterTown: "+.@v$);

    LOCATION$=.@v$;
    return;
}

// Warps home and updates LOCATION$
function	script	teleporthome	{
    warp "Save", 0, 0;
    .@i=array_find($@LOCMASTER_MAP$, getmap());
    if (.@i >= 0)
        EnterTown($@LOCMASTER_LOC$[.@i]);
    else
        debugmes("[ERROR] Invalid Town Map for Time Flask: %s", getmap());
    return;
}

