// Evol scripts.
// Author:
//    gumi
// Description:
//    checks player permissions
//    ** admins are implicitly everything

// administrator (GM 99)
function	script	is_admin	{
    return has_permission(PERM_USE_ALL_COMMANDS,
                          getarg(0, getcharid(CHAR_ID_ACCOUNT)));
}

// any staff member (GM 1+)
function	script	is_trusted	{
    return has_permission("show_client_version",
                          getarg(0, getcharid(CHAR_ID_ACCOUNT)));
}

// developer (GM 20+)
function	script	is_dev	{
    return has_permission(PERM_RECEIVE_REQUESTS,
                          getarg(0, getcharid(CHAR_ID_ACCOUNT)));
}

// senior developer (GM 40, GM 80+)
function	script	is_senior	{
    return can_use_command("@loadnpc",
                           getarg(0, getcharid(CHAR_ID_ACCOUNT)));
}

// event coordinator (GM 50+)
function	script	is_evtc	{
    return can_use_command("@monster",
                           getarg(0, getcharid(CHAR_ID_ACCOUNT)));
}

// game master (GM 60+)
function	script	is_gm	{
    return can_use_command("@jail",
                           getarg(0, getcharid(CHAR_ID_ACCOUNT)));
}
