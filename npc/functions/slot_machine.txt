// Slot Machine

function	script	SlotMachineSymbol	{
        switch (getarg(0)) {
        case 0:
            mesn "%%A";
            break;
        case 1:
            mesn "%%B";
            break;
        case 2:
            mesn "%%C";
            break;
        case 3:
            mesn "%%D";
            break;
        case 4:
            mesn "%%E";
            break;
        case 5:
            mesn "%%F";
            break;
        case 6:
            mesn "7";
            break;
        default:
            mesn "%%@";
            break;
        }
    }

function	script	SlotMachine	{
    mes "Pull the lever...";
    next;
    menu
        "Pull", L_Play,
        "Maybe later", L_close;

L_Play:
    if(countitem("CasinoCoins") < 1)
        goto L_NoCoin;
    delitem "CasinoCoins", 1;
    .@Temp1 = rand(7);
    .@Temp2 = rand(7);
    .@Temp3 = rand(7);
    //mes "Numbers: " + .@Temp1 + "/" + .@Temp2 + "/" + .@Temp3 + ".";
    SlotMachineSymbol(.@Temp1);
    SlotMachineSymbol(.@Temp2);
    SlotMachineSymbol(.@Temp3);
    next;

    if (.@Temp1 != .@Temp2)
        goto L_Lost;
    if (.@Temp2 != .@Temp3)
        goto L_Lost;
    if (.@Temp1 != .@Temp3)
        goto L_Lost;
    if (CSN < 9 && rand(6) < CSN)
        goto L_Jackpot;
    mes "Congratulations! You won!";
    mes "You get 10 casino coins";
    getitem CasinoCoins, 10;
    if (CSN < 9)
        CSN+=1;
    goto L_close;

L_Jackpot:
    mes "Congratulations! You won!";
    mes "However, the slot machine";
    mes "do not give you the coins!";
    next;
    mes "[Staff]";
    mes "\"I apologize for this problem.";
    mes "I see you are a huge client of";
    mes "ours, so I'll give you a Monocle";
    mes "as a token of apology.\"";
    getitem Monocle, 1;
    CSN = 9;
    goto L_close;

L_Lost:
    mes "You lost!";
    goto L_close;

L_NoCoin:
    mes "Insert coin";
    goto L_close;

L_close:
    closeclientdialog;
    return;
}
