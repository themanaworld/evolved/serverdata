// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 032-3: Outback Cave mobs
032-3,105,54,4,5	monster	Skeleton	1043,3,30000,100000
032-3,59,33,14,11	monster	Mountain Snake	1026,7,30000,100000
032-3,140,56,17,37	monster	Snake	1010,13,30000,100000
032-3,95,54,4,5	monster	Black Scorpion	1009,5,30000,100000
032-3,100,43,4,5	monster	Spider	1012,5,30000,100000
032-3,98,28,18,4	monster	Archant	1060,8,30000,100000
032-3,93,86,12,10	monster	Archant	1060,8,30000,100000
032-3,40,86,12,10	monster	Yellow Slime	1007,11,300000,1000000
032-3,61,61,17,9	monster	Cave Maggot	1056,11,30000,100000
032-3,0,0,0,0	monster	Bat	1017,25,30000,100000
