// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 002-4: Desert Mines mobs
002-4,39,37,7,5	monster	Cave Maggot	1056,5,100000,30000
002-4,91,41,8,10	monster	Angry Fire Goblin	1108,3,100000,30000
002-4,71,35,12,8	monster	Cave Maggot	1056,4,100000,30000
002-4,69,100,4,2	monster	Angry Fire Goblin	1108,2,100000,30000
002-4,68,57,13,12	monster	Angry Scorpion	1057,5,100000,30000
002-4,49,58,2,7	monster	Angry Scorpion	1057,5,100000,30000
002-4,38,76,7,11	monster	Angry Scorpion	1057,4,100000,30000
002-4,58,78,3,8	monster	Cave Maggot	1056,4,100000,30000
002-4,89,68,5,16	monster	Angry Scorpion	1057,4,100000,30000
002-4,84,93,9,8	monster	Cave Maggot	1056,4,100000,30000
002-4,72,77,10,7	monster	Angry Fire Goblin	1108,3,100000,30000
002-4,67,91,5,6	monster	Angry Scorpion	1057,4,100000,30000
002-4,58,95,3,6	monster	Cave Maggot	1056,4,100000,30000
002-4,63,100,1,2	monster	Angry Scorpion	1057,3,100000,30000
