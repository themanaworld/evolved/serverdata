// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 013-3: Woodland Hills Cave mobs
013-3,28,89,8,26	monster	Cave Snake	1021,10,120000,60000
013-3,61,22,1,1	monster	Fire Skull	1023,1,120000,60000
013-3,81,22,1,1	monster	Poison Skull	1024,1,120000,60000
013-3,109,31,3,11	monster	Black Scorpion	1009,20,50000,25000
013-3,119,84,13,19	monster	Spider	1012,20,120000,40000
013-3,176,23,2,3	monster	Spider	1012,10,100000,40000
013-3,82,75,1,1	monster	Fire Skull	1023,1,180000,60000
013-3,142,37,1,1	monster	Poison Skull	1024,1,180000,60000
013-3,79,134,10,8	monster	Black Scorpion	1009,5,80000,40000
013-3,71,169,19,8	monster	Black Scorpion	1009,5,80000,40000
013-3,64,85,4,10	monster	Black Scorpion	1009,10,80000,40000
013-3,81,92,1,10	monster	Black Scorpion	1009,10,80000,40000
013-3,163,84,16,18	monster	Snake	1010,30,120000,50000
013-3,162,85,16,18	monster	Spider	1012,30,120000,50000
013-3,172,77,9,12	monster	Black Scorpion	1009,15,120000,50000
013-3,145,36,10,7	monster	Red Slime	1008,25,80000,40000
013-3,167,37,11,5	monster	Snake	1010,20,120000,40000
013-3,165,25,8,5	monster	Black Scorpion	1009,15,120000,40000
013-3,71,39,1,1	monster	Snake	1010,2,40000,20000
013-3,159,102,1,2	monster	Yellow Slime	1007,2,80000,40000
013-3,67,22,1,1	monster	Maggot	1002,2,40000,20000
013-3,75,22,1,1	monster	Maggot	1002,2,40000,20000
013-3,76,39,3,2	monster	Black Scorpion	1009,8,40000,20000
013-3,65,39,3,2	monster	Red Slime	1008,8,40000,20000
013-3,62,33,1,0	monster	Poison Skull	1024,1,120000,60000
013-3,80,33,1,0	monster	Fire Skull	1023,1,120000,60000
013-3,75,46,1,0	monster	Poison Skull	1024,1,120000,60000
013-3,67,46,1,0	monster	Fire Skull	1023,1,120000,60000
