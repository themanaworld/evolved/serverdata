// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 045-1: Deep Snow Forest mobs
045-1,102,122,11,11	monster	Wolvern	1090,10,100000,30000
045-1,98,77,11,11	monster	Wolvern	1090,5,100000,30000
045-1,147,63,11,11	monster	Wolvern	1090,5,100000,30000
045-1,60,97,11,11	monster	Wolvern	1090,5,100000,30000
045-1,106,44,11,11	monster	Wolvern	1090,5,100000,30000
045-1,133,93,11,11	monster	Wolvern	1090,5,100000,30000
045-1,38,76,9,7	monster	Wolvern	1090,5,100000,30000
045-1,60,59,9,7	monster	Wolvern	1090,5,100000,30000
045-1,0,0,0,0	monster	Squirrel	1038,40,100000,30000
045-1,0,0,0,0	monster	Reinboo	1094,30,100000,30000
045-1,0,0,0,0	monster	Fluffy	1020,40,100000,30000
045-1,0,0,0,0	monster	White Bell	1095,5,100000,30000
045-1,47,145,12,12	monster	Ice Goblin	1058,4,100000,30000
045-1,150,126,12,12	monster	Ice Goblin	1058,4,100000,30000
045-1,104,149,39,5	monster	Ice Goblin	1058,4,100000,30000
