// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 004-4: Pirate Caves Second Floor mobs
004-4,36,62,2,10	monster	Grenadier	1121,1,100000,30000
004-4,37,61,2,10	monster	Swashbuckler	1120,2,100000,30000
004-4,62,52,6,3	monster	Thug	1119,2,100000,30000
004-4,34,87,6,3	monster	Thug	1119,2,100000,30000
004-4,37,62,3,11	monster	Thug	1119,2,100000,30000
004-4,34,88,5,3	monster	Swashbuckler	1120,2,100000,30000
004-4,63,52,5,3	monster	Swashbuckler	1120,2,100000,30000
004-4,62,52,5,2	monster	Grenadier	1121,1,100000,30000
004-4,34,88,5,2	monster	Grenadier	1121,1,100000,30000
