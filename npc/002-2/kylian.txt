



-	script	#businessmanNTconfig	NPC32767,{
    end;

OnInit:
    // Nibble 4
    // Used in Random Quest later as a Nibble
    $@knowYanisNT = (1 << 16);
    $@knowLatoyNT = (1 << 17);
    $@knowWeellosNT = (1 << 18);
    $@knowCasinoNT = (1 << 19);

    // Random Quests for Later
    setarray $@random_quests$, "None", "Falkurn", "Old Wizard", "Baktar";
    $@delivery_money = 100;
    $@delivery_exp_mod = 10;
    end;
}
002-2,44,107,0	script	#KylianOut	NPC32767,1,1,{
    @KylianNTLicense = 0;
    @KylianNTSightSeeing = 0;
    @KylianNTCasino = 0;
    end;
}
002-2,43,101,0	script	Kylian	NPC193,{
    @acorn_amount = 12;
    @suitcase_money = 100;
    @suitcase_exp = 50;
    @license_money = 50;
    @license_exp = 20;
    @sightseeing_money = 50;
    @sightseeing_exp = 20;
    @food_money = 50;
    @food_exp = 20;
    @clothes_money = 50;
    @clothes_exp = 20;
    @fun_money = 50;
    @fun_exp = 20;

    if (QL_KYLIAN == 15) goto L_Done;
    if (QL_KYLIAN >= 11) goto L_Helping;
    if (QL_KYLIAN == 10) goto L_OfferedJob;
    if (QL_KYLIAN == 9) goto L_Shop;
    if (@KylianNTCasino) goto L_CasinoDone;
    if (QL_KYLIAN == 8) goto L_DesertHat;
    if (QL_KYLIAN == 7) goto L_Clothes;
    if (@KylianNTSightSeeing) goto L_SightSeeingDone;
    if (QL_KYLIAN == 6) goto L_Casino;
    if (QL_KYLIAN == 5) goto L_Food;
    if (@KylianNTLicense) goto L_LicenseDone;
    if (QL_KYLIAN == 4) goto L_SightSeeing;
    if (QL_KYLIAN == 3) goto L_ShopLicense;
    if (QL_KYLIAN >= 1) goto L_Suitcase;

    mes "[Kylian]";
    mes "\"Ah! Are you the room service? I've some requests.\"";
    menu
        "Sure... What can I do for you?",L_Luggage,
        "No I'm not!",L_Next;

L_Next:
    mes "[Kylian]";
    mes "\"Too bad. Would you be interested in earning some quick money with some errands regardless?\"";
    menu
        "I'm a great adventurer! I don't do petty errands.",L_close,
        "No. I'm busy.",L_close,
        "Well... OK.",L_Luggage;

L_Luggage:
    mes "[Kylian]";
    mes "\"I need you to get my luggage from the docks. Just show this paper to the sailor who's watching the luggage.\"";
    mes "He gives you his ticket, which you promptly store in a safe pocket outside of your inventory.";
    QL_KYLIAN = 1;
    goto L_close;

L_Suitcase: // QL_KYLIAN >= 1, but below 3
    mes "[Kylian]";
    mes "\"Did you get my luggage from the docks?\"";
    if (QL_KYLIAN != 2)
        goto L_close;
    menu
        "Here it is.",L_Continue,
        "Don't worry; I'm on my way.",L_close;

L_Continue:
    if (countitem("LeatherSuitcase") < 1)
        goto L_NoItem;
    delitem "LeatherSuitcase", 1;
    Zeny = Zeny + @suitcase_money;
    getitem "Acorn", @acorn_amount;
    getexp @suitcase_exp, 0;
    QL_KYLIAN = 3;
    next;
    mes "[Kylian]";
    mes "\"Ah! Very good. I have some urgent paperwork that I've been needing to attend to.\"";
    mes "He gives you some money.";
    next;
    mes "[Kylian]";
    mes "\"Before you go, I also happen to have some acorns left over from my trip.\"";
    mes "\"You can have them if you wish; they're rather tasty.\"";
    mes "If, however, you don't like them, you could take them to the Tulimshar bakery. I heard they use them to make a special kind of flour.\"";
    next;
    goto L_ShopLicense;

L_ShopLicense: // QL_KYLIAN == 3
    mes "[Kylian]";
    mes "\"I'm a salesman and came to Tulimshar because I'm thinking about establishing a shop here.\"";
    mes "\"While I'm going through my papers, could you find out whom I have to talk to about opening up a shop in this city?\"";
    if (!(QUEST_NorthTulimshar & $@knowYanisNT))
        goto L_close;
    menu
        "You need to go and talk to Yanis in the government building.",L_GovBuild,
        "I'll see what I can do.",L_close;

L_GovBuild:
    Zeny = Zeny + @license_money;
    getexp @license_exp, 0;
    QL_KYLIAN = 4;
    @KylianNTLicense = 1;
    mes "[Kylian]";
    mes "\"Ah... excellent! That's very helpful. Could you tell me how to get to that building?\"";
    mes "You give him directions to the building.";
    next;
    goto L_LicenseDone;

L_LicenseDone: // the player didn't log out yet after telling about Yanis
    mes "[Kylian]";
    mes "\"I need to prepare my papers now. I might have some more questions later on though.\"";
    goto L_close;

L_SightSeeing: // QL_KYLIAN == 4 and logged out sometime between getting to that state and now
    mes "[Kylian]";
    mes "\"You came here at just the right moment! I have finished my business affairs, and I think I should use my time here to learn a bit about the culture in the area. Can you tell me if there are any historical places or landmarks to visit?\"";
    if (!(QUEST_NorthTulimshar & $@knowWeellosNT))
        goto L_close;
    menu
        "There's a very old building not far from here.",L_HistBuild,
        "I don't know, but I'll have a look around.",L_close;

L_HistBuild:
    Zeny = Zeny + @sightseeing_money;
    getexp @sightseeing_exp, 0;
    QL_KYLIAN = 5;
    mes "[Kylian]";
    mes "\"This sounds interesting. Please tell me the way.\"";
    mes "You tell him how to get to the historic building.";
    next;
    goto L_Food;

L_Food: // QL_KYLIAN == 5
    mes "[Kylian]";
    mes "\"While I'm out, I could also get something to eat. Do you have any suggestions for local cuisine?\"";
    if (QL_FIERI < 4) // didn't yet help Fieri make Tonori Delight
        goto L_close;
    menu
        "A man named Fieri makes a tasty Tonori Delight over at the castle.",L_SoupBer,
        "No idea. I'll try to find out.",L_close;

L_SoupBer:
    Zeny = Zeny + @food_money;
    getexp @food_exp, 0;
    QL_KYLIAN = 6;
    @KylianNTSightSeeing = 1;
    goto L_SightSeeingDone;

L_SightSeeingDone:
    mes "[Kylian]";
    mes "\"I'm going to see the historic building you told me about and try the local food at the castle. Thank you for the suggestions.\"";
    goto L_close;

L_Casino: // QL_KYLIAN == 6 and logged out sometime between getting to that state and now
    mes "[Kylian]";
    mes "\"Hello. I just came back from my sight-seeing tour, and this Tonori Delight really was delicious. I wonder what it's made of...\"";
    next;
    mes "\"However, I was wondering if you know about some evening attractions. Maybe somewhere to have fun?\"";
    if (!(QUEST_NorthTulimshar & $@knowCasinoNT))
        goto L_close;
    menu
        "Of course! The casino!",L_CasinoFound,
        "Not really.",L_close;

L_CasinoFound:
    Zeny = Zeny + @casino_money;
    getexp @casino_exp, 0;
    QL_KYLIAN = 7;
    mes "[Kylian]";
    mes "\"Oh! There's a casino in this city? That's wonderful! Where can I find it?\"";
    mes "You explain how to get to the casino.";
    next;
    goto L_Clothes;

L_Clothes: // QL_KYLIAN == 7
    mes "[Kylian]";
    mes "\"I should acquire proper clothing before I go to the casino tonight. Do you know a reputable shop where high-quality clothing is sold?\"";
    if (!(QUEST_NorthTulimshar & $@knowLatoyNT))
        goto L_close;
    menu
        "Latoy's shop. It's on the west side of Tulimshar.",L_HarborDistrict,
        "Hmmm... I don't know.",L_close;

L_HarborDistrict:
    Zeny = Zeny + @clothes_money;
    getexp @clothes_exp, 0;
    QL_KYLIAN = 8;
    @KylianNTCasino = 1;
    mes "You explain about Latoy and the quality of his shop.";
    next;
    mes "[Kylian]";
    mes "\"This seems to be exactly what I need. Thanks a lot.\"";
    goto L_close;

L_CasinoDone:
    mes "[Kylian]";
    mes "\"I'm looking forward to going to the casino tonight. See me tomorrow, and I might have more requests.\"";
    goto L_close;

L_DesertHat: // QL_KYLIAN == 8
    mes "Kylian looks a bit tired.";
    next;
    mes "[Kylian]";
    mes "\"That was an interesting night. Thanks for your suggestion to visit the casino.\"";
    next;
    getinventorylist;
    mes "\"You helped me out a lot, so I bought a souvenir for you while I was out shopping.\"";
    if ((checkweight("DesertHat", 1) == 0) || (@inventorylist_count == 100))
        goto L_Inventory;
    getitem "DesertHat", 1;
    QL_KYLIAN = 9;
    next;
    goto L_close;

L_Shop:
    mes "[Kylian]";
    mes "\"The Council of Wizards approved my shop license!\"";
    mes "\"I purchased a store just across the way from the inn.\"";
    next;
    mes "\"If you are looking for work, go see the shop keeper I have working for me there.\"";
    mes "\"Mention your name, and he will know I sent you.\"";
    QL_KYLIAN = 10;
    goto L_close;

L_OfferedJob:
    mes "[Kylian]";
    mes "\"I spoke with the shopkeeper, and he said you haven't stopped in yet.\"";
    next;
    mes "\"I wish you would consider working for me.\"";
    next;
    mes "\"KPS needs a determined, experienced adventurer like you.\"";
    goto L_close;

L_Helping:
    mes "[Kylian]";
    mes "\"I've heard you decided to come work for me. That's great news!\"";
    mes "\"Keep up the good work, and I might have something a little extra for you.\"";
    goto L_close;

L_Done:
    mes "[Kylian]";
    mes "\"Thanks a bunch, business is booming!\"";
    mes "\"Please, take this for all your hard work.\"";
    next;
    Zeny = Zeny + $@delivery_money;
    getexp ($@delivery_exp_mod * BaseLevel), 0;
    @run_cnt = 0;
    callfunc "SetKylianRunCnt";
    QL_KYLIAN = 12;
    mes "[" + $@delivery_money + " money]";
    mes "[" + ($@delivery_exp_mod * BaseLevel) + " experience points]";
    next;
    goto L_close;

L_Inventory:
    mes "[Kylian]";
    mes "\"Oh my. You're carrying quite a lot. Come back when you have more room.\"";
    goto L_close;

L_NoItem:
    mes "[Kylian]";
    mes "\"I don't see it! Are you trying to tease me? This isn't funny!\"";
    goto L_close;

L_close:
    @acorn_amount = 0;
    @suitcase_money = 0;
    @suitcase_exp = 0;
    @license_money = 0;
    @license_exp = 0;
    @sightseeing_money = 0;
    @sightseeing_exp = 0;
    @food_money = 0;
    @food_exp = 0;
    @clothes_money = 0;
    @clothes_exp = 0;
    @fun_money = 0;
    @fun_exp = 0;
    @inventorylist_count = 0;
    // NOT set to zero: @KylianNTLicense, @KylianNTSightSeeing and @KylianNTCasino
    // those are used to check if the player logged out in the meanwhile
    close;
}
function	script	KylianDebug	{
    @run = ((QUEST_NorthTulimshar & TWOBIT_8_MASK) >> TWOBIT_8_SHIFT);
    @run_cnt = ((QUEST_NorthTulimshar & TWOBIT_9_MASK) >> TWOBIT_9_SHIFT);
    goto L_Menu;

L_Menu:
    mes "[Kylian Debug]";
    menu
        "Show Quest State", L_ShowState,
        "Set Quest State", L_SetState,
        "Reset Timer to Now", L_WorkTimer,
        "Reset Timer + 24 Hours", L_MoreWorkTimer,
        "Close.", L_close;

L_ShowState:
    mes "State: " + QL_KYLIAN;
    mes "Timer: " + Kylian_Timer;
    mes "Time: " + gettimetick(2);
    mes "Elapsed Time: " + (gettimetick(2) - Kylian_Timer);
    mes "Reset Timer: 86400 > " + (gettimetick(2) - Kylian_Timer);
    mes "Run: " + @run;
    mes "Run NPC: " + $@random_quests$[@run];
    mes "Run Count: " + @run_cnt;
    goto L_Menu;

L_SetState:
    mes "\"Input the quest state desired.\"";
    input QL_KYLIAN;
    goto L_Menu;

L_WorkTimer:
    Kylian_Timer = gettimetick(2);
    goto L_Menu;

L_MoreWorkTimer:
    Kylian_Timer = (gettimetick(2) - 86401);
    goto L_Menu;

L_close:
    return;
}
002-2,40,101,0	script	KylianDebug#1	NPC193,{
    callfunc "KylianDebug";
    close;
OnInit:
    if (!debug)
        disablenpc "KylianDebug#1";
    end;
}
002-2,118,89,0	script	KylianDebug#2	NPC193,{
    callfunc "KylianDebug";
    close;
OnInit:
    if (!debug)
        disablenpc "KylianDebug#2";
    end;
}
