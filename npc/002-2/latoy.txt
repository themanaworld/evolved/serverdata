002-2,86,93,0	shop	#LatoyShop	NPC32767,544:8000,868:10000,1172:3000,720:16000
002-2,86,93,0	script	Latoy	NPC106,{
    QUEST_NorthTulimshar = QUEST_NorthTulimshar | $@knowLatoyNT;

    mes "[Latoy]";
    mes "\"Is there something I can help you with?\"";
    if (QL_KYLIAN != 7)
        goto L_Shop;
    next;
    mes "Maybe this is the kind of shop Kylian is looking for?";
    goto L_Shop;

L_Shop:
    mes "[Latoy]";
    mes "\"How would you like to browse my wares?\"";
    menu
        "Yes.", L_LatoyShop,
        "No.", L_close;

L_LatoyShop:
    close2;
    shop "#LatoyShop";

L_close:
    close;
}
