
010-2,68,87,0	script	Doug	NPC113,{
    mes "[Doug]";
    mes "\"This room is too dark. I want to brighten it up.\"";
    next;

    @dq_level = 15;
    @dq_cost = 10;
    @dq_count = 5;
    @dq_name$ = "CaveSnakeLamp";
    @dq_friendly_name$ = "Cave Snake Lamps";
    @dq_money = 1000;
    @dq_exp = 1500;

    callfunc "DailyQuest";

    next;
    mes "[Doug]";
    mes "\"Too bad these lamps wear off after a while...\"";
    close;
}
