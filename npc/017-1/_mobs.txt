// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 017-1: Woodland Hills mobs
017-1,0,0,0,0	monster	Clover Patch	1037,2,0,1000
017-1,99,30,1,0	monster	Clover Patch	1037,1,150000,50000
017-1,29,29,9,6	monster	Gamboge Plant	1031,1,0,25000
017-1,91,25,38,4	monster	Log Head	1025,3,5000,60000
017-1,31,48,9,6	monster	Log Head	1025,3,5000,60000
017-1,0,0,0,0	monster	Butterfly	1055,10,30,20
017-1,0,0,0,0	monster	Bat	1017,5,0,0
017-1,0,0,0,0	monster	Fire Goblin	1011,5,0,0
017-1,0,0,0,0	monster	Mouboo	1028,5,0,0
017-1,0,0,0,0	monster	Pink Flower	1014,5,0,0
017-1,0,0,0,0	monster	Spiky Mushroom	1019,5,0,0
017-1,0,0,0,0	monster	Evil Mushroom	1013,10,0,0
017-1,0,0,0,0	monster	Alizarin Plant	1032,3,0,0
017-1,0,0,0,0	monster	Gamboge Plant	1031,3,0,0
017-1,0,0,0,0	monster	Cobalt Plant	1030,3,0,0
017-1,0,0,0,0	monster	Mauve Plant	1029,5,0,0
017-1,0,0,0,0	monster	Silkworm	1035,10,0,0
017-1,0,0,0,0	monster	Squirrel	1038,30,20,10
