
017-1,176,59,0	script	#FlowerPentagram5	NPC400,{
    if (getq(MagicQuest_DarkMage) >= 31) goto L_PlacedFifthFlower;

    if (getq(MagicQuest_DarkMage) == 30) goto L_FifthFlower;

    message strcharinfo(0), "Something is odd about this place.";
    end;

L_FifthFlower:
    mes "This has to be the spot for the last magical flower. When you place it, the pentagram will be completed.";
    menu
        "Place the flower.", L_PlaceTheFlower,
        "Leave.", L_close;

L_PlaceTheFlower:
    if (isin("017-1", 175, 58, 177, 60))
        goto L_Place;

    mes "This isn't working, you're too far away.";
    goto L_close;

L_Place:
    set @localMonsterCount,
        mobcount("013-1", "#FlowerPentagram5::OnSquirrelDeath") +
        mobcount("013-1", "#FlowerPentagram5::OnMushroomDeath") +
        mobcount("013-1", "#FlowerPentagram5::OnBlueparDeath") +
        mobcount("013-1", "#FlowerPentagram5::OnMoubooDeath") +
        mobcount("013-1", "#FlowerPentagram5::OnScorpionDeath") +
        mobcount("013-1", "#FlowerPentagram5::OnRScorpionDeath") +
        mobcount("013-1", "#FlowerPentagram5::OnAScorpionDeath") +
        mobcount("013-1", "#FlowerPentagram5::OnBScorpionDeath");
    if (@localMonsterCount > 4)
        goto L_MonstersAlive;

    mes "Nervous about what's going to happen, you take the last of the flowers and put it into its place.";
    next;
    mes "Tension builds up around you, much stronger than before. You can feel it compressing your head, and a liquid warmth spreads under your nose, leaving a metallic taste on your lips.";
    heal -5, 0;
    next;
    callfunc "FlowerPentagramCount";
    if (($@Flower_Pentagram_Time > gettimetick(2) - 3600) && ($@Flower_Pentagram_Mobcount > 15))
        goto L_TooEarly;
    $@Flower_Pentagram_Time = gettimetick(2);
    if (countitem("RedSummonFlower") < 1)
        goto L_NoFlower;
    delitem "RedSummonFlower", 1;

    mes "The pressure builds to the point where you don't think you can endure it any longer; the fabric that binds your consciousness together seemingly unravels.";
    setq(MagicQuest_DarkMage, 31);
    close2;
    areamonster "017-1", 170, 55, 190, 75, "", 1105, 3, "#FlowerPentagram5::OnSquirrelDeath";
    areamonster "017-1", 170, 55, 190, 75, "", 1106, 2, "#FlowerPentagram5::OnMushroomDeath";
    areamonster "017-1", 170, 55, 190, 75, "", 1107, 3, "#FlowerPentagram5::OnBlueparDeath";
    areamonster "017-1", 170, 55, 190, 75, "", 1104, 2, "#FlowerPentagram5::OnMoubooDeath";
    areamonster "017-1", 170, 55, 190, 75, "", 1003, 3, "#FlowerPentagram5::OnScorpionDeath";
    areamonster "017-1", 170, 55, 190, 75, "", 1004, 2, "#FlowerPentagram5::OnRScorpionDeath";
    areamonster "017-1", 170, 55, 190, 75, "", 1057, 2, "#FlowerPentagram5::OnAScorpionDeath";
    areamonster "017-1", 170, 55, 190, 75, "", 1009, 2, "#FlowerPentagram5::OnBScorpionDeath";
    @value = 15;
    callfunc "QuestSagathaAnnoy";
    @value = 0;
    donpcevent "#FlowerPentagram::OnSummon";
    end;

OnSquirrelDeath:
    fix_mobkill(1105);
    end;

OnMushroomDeath:
    fix_mobkill(1106);
    end;

OnBlueparDeath:
    fix_mobkill(1107);
    end;

OnMoubooDeath:
    fix_mobkill(1104);
    end;

OnScorpionDeath:
    fix_mobkill(1003);
    end;

OnRScorpionDeath:
    fix_mobkill(1004);
    end;

OnAScorpionDeath:
    fix_mobkill(1057);
    end;

OnBScorpionDeath:
    fix_mobkill(1009);
    end;

L_TooEarly:
    mes "Suddenly, everything calms down again and the tension vanishes. You find yourself confused about what's going on.";
    next;
    mes "You pick up the flower again.";
    next;
    mes "Looking at the place more closely, it seems another summoning has happened just before, leaving the pathway between this world and the astral plane drained and exhausted.";
    next;
    mes "You should wait a while until it recharges, or go and search for the creatures that were summoned.";
    goto L_close;

L_MonstersAlive:
    mes "As you get closer to the place, you feel an unnerving presence.";
    mes "This place has recently been used to summon something! And the beings are still nearby!";
    next;
    mes "You should get rid of them before attempting the summoning yourself.";
    goto L_close;

L_NoFlower:
    mes "You reach into your pocket, but you can't find the flower! You're so close to completing your task.";
    mes "How could you lose the last flower? If you can't get it back, you have to tell Orum.";
    goto L_close;

L_PlacedFifthFlower:
    mes "This is where you placed the last of Orum's magical flowers.";
    mes "However, now that the summoning spell has been cast, everything is back to normal here.";
    goto L_close;

L_close:
    @localMonsterCount = 0;
    close;
}
