
035-2,39,29,0	script	#KogaExit	NPC45,0,1,{
    mes "Disembark the ferry for "+$@MainDocks$[$@MainCurrentDock]+"?";
    menu
        "Yes.", L_Leave,
        "Nevermind", L_close;

L_Leave:
    if ($@MainCurrentDock == 0)
        goto L_Hurnscald;
    if ($@MainCurrentDock == 1)
        goto L_Nivalis;
    if ($@MainCurrentDock == 2)
        goto L_Tulimshar;
    goto L_close;

L_Tulimshar:
    EnterTown("Tulim");
    warp "001-1", 63, 73;
    goto L_close;

L_Hurnscald:
    EnterTown("Hurns");
    warp "008-1", 136, 64;
    goto L_close;

L_Nivalis:
    EnterTown("Nival");
    warp "031-1", 95, 109;
    goto L_close;

L_close:
    closeclientdialog;
    close;
}
