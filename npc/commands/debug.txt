
function	script	Debug	{
    goto L_Begin;

L_Begin:
    mes "What do you want to do?";
    mes "Your range: "+readbattleparam(getcharid(3), UDT_ATKRANGE);
    mes "Bow range: "+getiteminfo(Bow, ITEMINFO_RANGE);
    menu
        "Reset stat points.", L_ResetStatusPoints,
        "Change my level.", L_Level,
        "Change other stuff.", L_Stats,
        "Change my basic skills.", L_BasicSkills,
        "Change my focus skills.", L_FocusSkills,
        "Change my magic skills.", L_MagicSkills,
        "Add everything.", L_AddAll,
        "Reset everything.", L_ResetAll,
        "Close.", L_close;

L_Stats:
    mesc l("Stats: %s", col(b("@str/@agi/@vit/@int/@dex/@luk/@allstats"),3));
    mesc l("GP: %s", col(b("@zeny"),3));
    mesc l("Items: %s", col(b("/createitems"),3));
    mesc l("Hide from monsters: %s", col(b("@monsterignore or @safe"),3));
    mesc l("Reset Stats: %s", col(b("@streset"),3));
    next;
    goto L_Begin;

L_Level:
    mes "What level do you want to be (min: 1 - max: 140)?";
    input @lvl;
    if (@lvl < 1)
        goto L_LevelTooLow;
    if (@lvl > 140)
        goto L_LevelTooHigh;
    if (BaseLevel == @lvl)
        goto L_SameLevel;
    gmlog sprintf("Level set from %d to %d", BaseLevel, @lvl);
    BaseLevel = @lvl;
    resetstatus;
    mes "You are now level " + BaseLevel + ".";
    goto L_Begin;

L_LevelTooLow:
    mes "Bad choice. Minimum level is 1.";
    next;
    goto L_Begin;

L_LevelTooHigh:
    mes "Bad choice. Maximum level is 140.";
    next;
    goto L_Begin;

L_SameLevel:
    mes "You already are level " + @lvl + ".";
    resetstatus;
    next;
    goto L_Begin;

L_ResetStatusPoints:
    resetstatus;
    mes "Stats successfully resetted.";
    next;
    goto L_Begin;

L_BasicSkills:
    adddefaultskills();
    goto L_Begin;

L_FocusSkills:
    @pool = getskilllv(SKILL_POOL);
    @mallard = getskilllv(SKILL_MALLARDS_EYE);
    @brawling = getskilllv(SKILL_BRAWLING);
    @speed = getskilllv(SKILL_SPEED);
    @poison = getskilllv(SKILL_RESIST_POISON);
    @astralsoul = getskilllv(SKILL_ASTRAL_SOUL);
    @raging = getskilllv(SKILL_RAGING);

    mes "Your focusing skill level is " + @pool + ".";
    mes "Your mallard's eye skill level is " + @mallard + ".";
    mes "Your brawling skill level is " + @brawling + ".";
    mes "Your speed skill level is " + @speed + ".";
    mes "Your resist poison skill level is " + @poison + ".";
    mes "Your astral soul skill level is " + @astralsoul + ".";
    mes "Your raging skill level is " + @raging + ".";
    next;
    mes "";
    mes l("Focus list:");
    mesc l("%s : %s", l("Mallard's Eye"),
          (isfocused(SKILL_MALLARDS_EYE) ? l("Yes") : l("No")));
    mesc l("%s : %s", l("Brawling"),
          (isfocused(SKILL_BRAWLING) ? l("Yes") : l("No")));
    mesc l("%s : %s", l("Speed"),
          (isfocused(SKILL_SPEED) ? l("Yes") : l("No")));
    mesc l("%s : %s", l("Resist Ailment"),
          (isfocused(SKILL_RESIST_POISON) ? l("Yes") : l("No")));
    mesc l("%s : %s", l("Astral Soul"),
          (isfocused(SKILL_ASTRAL_SOUL) ? l("Yes") : l("No")));
    mesc l("%s : %s", l("Raging"),
          (isfocused(SKILL_RAGING) ? l("Yes") : l("No")));
    .@t=getactivatedpoolskilllist();
    menuint
        l("Back"), -3,
        l("Focus Skill +"), -2,
        rif(getskilllv(SKILL_POOL), l("Focus Skill -")), -1,
        l("Add all focus skills"), 0,
        l("Remove all focus skills"), -4,
        ("Toggle Focus - Mallards Eye"), SKILL_MALLARDS_EYE,
        ("Toggle Focus - Brawling"), SKILL_BRAWLING,
        ("Toggle Focus - Speed"), SKILL_SPEED,
        ("Toggle Focus - Resist Ailment"), SKILL_RESIST_POISON,
        ("Toggle Focus - Astral Soul"), SKILL_ASTRAL_SOUL,
        ("Toggle Focus - Raging"), SKILL_RAGING;
    mes "";
    switch (@menuret) {
    case -3: goto L_Begin;
    case -2:
        skill SKILL_POOL, @pool+1, 0; gmlog("Focus+"); break;
    case -1:
        skill SKILL_POOL, max(0, @pool-1), 0; gmlog("Focus-"); break;
    case 0:
        updateskill SKILL_MALLARDS_EYE, 9;
        updateskill SKILL_BRAWLING, 9;
        updateskill SKILL_SPEED, 9;
        updateskill SKILL_RESIST_POISON, 9;
        updateskill SKILL_ASTRAL_SOUL, 9;
        updateskill SKILL_RAGING, 9;
        gmlog("All focus to max");
        break;
    case -4:
        skill SKILL_POOL, 0, 0;
        updateskill SKILL_MALLARDS_EYE, 0;
        updateskill SKILL_BRAWLING, 0;
        updateskill SKILL_SPEED, 0;
        updateskill SKILL_RESIST_POISON, 0;
        updateskill SKILL_ASTRAL_SOUL, 0;
        updateskill SKILL_RAGING, 0;
        gmlog("All focus reset");
        break;
    default:
        if (FOCUSING & getpoolskillFID(@menuret)) {
            unpoolskill(@menuret);
            mesc "Focus removed", 1;
        } else {
            .@s = poolskill(@menuret);
            if (.@s)
                mesc "Focus added", 2;
            else
                mesc sprintf("Impossible to focus. You can only focus %d skills at a time.", .@t), 1;
        }
    }
    goto L_FocusSkills;

L_MagicSkills:
    @general = getskilllv(SKILL_MAGIC);
    @life = getskilllv(SKILL_MAGIC_LIFE);
    @war = getskilllv(SKILL_MAGIC_WAR);
    @trans = getskilllv(SKILL_MAGIC_TRANSMUTE);
    @nature = getskilllv(SKILL_MAGIC_NATURE);
    @astral = getskilllv(SKILL_MAGIC_ASTRAL);
    @dark = getskilllv(SKILL_MAGIC_DARK);
    menu
        "Overview of my magical skills.", L_MagicSkillsOverview,
        "Get magic skills.", L_ChangeMagicSkills,
        "Get magic experience.", L_MagicExperience,
        "All magic skills to their maximum level and maximum magic experience.", L_GetAllMagic,
        "Reset magic skills and experience.", L_ResetMagicSkills,
        "Back to the main menu.", L_Begin,
        "Close.", L_close;

L_MagicSkillsOverview:
    mes "Your current magic exp is "+MAGIC_EXP;
    mes "Your level in the general magic skill is " + @general + ".";
    mes "Your level in the life magic skill is " + @life + ".";
    mes "Your level in the war magic skill is " + @war + ".";
    mes "Your level in the transmutation magic skill is " + @trans + ".";
    mes "Your level in the nature magic skill is " + @nature + ".";
    mes "Your level in the astral magic skill is " + @astral + ".";
    mes "Your level in the dark magic skill is " + @dark + ".";
    next;
    goto L_MagicSkills;

L_MagicExperience:
    mes "Your current magic experience is " + MAGIC_EXP + ".";
    if (@general == 0
        && @life == 0
        && @war == 0
        && @trans == 0
        && @nature == 0
        && @astral == 0
        && @dark == 0)
            goto L_NoMagicSkills;

    goto L_ChangeMagicExperience;

L_NoMagicSkills:
    mes "You can't have magic experience, since you have no magic skills yet.";
    goto L_MagicSkills;

L_ChangeMagicExperience:
    mes "Set the desired magic experience (min: 0 - max: 65535).";
    input @value;
    if (@value < 0 || @value > 65535)
        goto L_WrongMagicExperience;
    MAGIC_EXP = @value;
    mes "You now have " + MAGIC_EXP + " magic experience points.";
    goto L_MagicSkills;

L_WrongMagicExperience:
    mes "Wrong value informed. Aborting.";
    goto L_MagicSkills;

L_ChangeMagicSkills:
    menu
        "General Magic.", L_ChangeGeneralMagicSkill,
        "Life Magic.", L_ChangeLifeMagicSkill,
        "War Magic.", L_ChangeWarMagicSkill,
        "Transmutation Magic.", L_ChangeTransmutationMagicSkill,
        "Nature Magic.", L_ChangeNatureMagicSkill,
        "Astral Magic.", L_ChangeAstralMagicSkill,
        "Dark Magic.", L_ChangeDarkMagicSkill,
        "Back to the magic skills menu.", L_MagicSkills,
        "Close.", L_close;

L_ChangeGeneralMagicSkill:
    mes "Your level in the general magic skill is " + @general + ". What do you want to do?";
    menu
        "Get level 0.", L_Next4,
        "Get level 1.", L_ChangeGeneralMagicSkill1,
        "Get level 2.", L_ChangeGeneralMagicSkill2;

L_Next4:
    if (@menu == 1)
        updateskill SKILL_MAGIC, 0;
    mes "General Magic skill changed to level 0.";
    next;
    goto L_MagicSkills;

L_ChangeGeneralMagicSkill1:
    updateskill SKILL_MAGIC, 1;
    mes "General Magic skill changed to level 1.";
    next;
    goto L_MagicSkills;

L_ChangeGeneralMagicSkill2:
    updateskill SKILL_MAGIC, 2;
    if (MAGIC_EXP < 100)
        MAGIC_EXP = 100;
    mes "General Magic skill changed to level 2.";
    next;
    goto L_MagicSkills;

L_ChangeLifeMagicSkill:
    mes "Your level in the life magic skill is " + @life + ". What do you want to do?";
    menu
        "Get level 0.", L_Next5,
        "Get level 1.", L_ChangeLifeMagicSkill1,
        "Get level 2.", L_ChangeLifeMagicSkill2;

L_Next5:
    if (@menu == 1)
        updateskill SKILL_MAGIC_LIFE, 0;
    mes "Life Magic skill changed to level 0.";
    next;
    goto L_MagicSkills;

L_ChangeLifeMagicSkill1:
    updateskill SKILL_MAGIC_LIFE, 1;
    mes "Life Magic skill changed to level 1.";
    next;
    goto L_MagicSkills;

L_ChangeLifeMagicSkill2:
    updateskill SKILL_MAGIC_LIFE, 2;
    if (MAGIC_EXP < 100)
        MAGIC_EXP = 100;
    mes "Life Magic skill changed to level 2.";
    next;
    goto L_MagicSkills;

L_ChangeWarMagicSkill:
    mes "Your level in the war magic skill is " + @war + ". What do you want to do?";
    menu
        "Get level 0.", L_Next6,
        "Get level 1.", L_ChangeWarMagicSkill1,
        "Get level 2.", L_ChangeWarMagicSkill2;

L_Next6:
    if (@menu == 1)
        updateskill SKILL_MAGIC_WAR, 0;
    mes "War Magic skill changed to level 0.";
    next;
    goto L_MagicSkills;

L_ChangeWarMagicSkill1:
    updateskill SKILL_MAGIC_WAR, 1;
    mes "War Magic skill changed to level 1.";
    next;
    goto L_MagicSkills;

L_ChangeWarMagicSkill2:
    updateskill SKILL_MAGIC_WAR, 2;
    if (MAGIC_EXP < 100)
        MAGIC_EXP = 100;
    mes "War Magic skill changed to level 2.";
    next;
    goto L_MagicSkills;

L_ChangeTransmutationMagicSkill:
    mes "Your level in the transmutation magic skill is " + @trans + ". What do you want to do?";
    menu
        "Get level 0.", L_Next7,
        "Get level 1.", L_ChangeTransmutationMagicSkill1,
        "Get level 2.", L_ChangeTransmutationMagicSkill2;

L_Next7:
    if (@menu == 1)
        updateskill SKILL_MAGIC_TRANSMUTE, 0;
    mes "Transmutation Magic skill changed to level 0.";
    next;
    goto L_MagicSkills;

L_ChangeTransmutationMagicSkill1:
    updateskill SKILL_MAGIC_TRANSMUTE, 1;
    mes "Transmutation Magic skill changed to level 1.";
    next;
    goto L_MagicSkills;

L_ChangeTransmutationMagicSkill2:
    updateskill SKILL_MAGIC_TRANSMUTE, 2;
    if (MAGIC_EXP < 100)
        MAGIC_EXP = 100;
    mes "Transmutation Magic skill changed to level 2.";
    next;
    goto L_MagicSkills;

L_ChangeNatureMagicSkill:
    mes "Your level in the nature magic skill is " + @nature + ". What do you want to do?";
    menu
        "Get level 0.", L_Next8,
        "Get level 1.", L_ChangeNatureMagicSkill1,
        "Get level 2.", L_ChangeNatureMagicSkill2;

L_Next8:
    if (@menu == 1)
        updateskill SKILL_MAGIC_NATURE, 0;
    mes "Nature Magic skill changed to level 0.";
    next;
    goto L_MagicSkills;

L_ChangeNatureMagicSkill1:
    updateskill SKILL_MAGIC_NATURE, 1;
    mes "Nature Magic skill changed to level 1.";
    next;
    goto L_MagicSkills;

L_ChangeNatureMagicSkill2:
    updateskill SKILL_MAGIC_NATURE, 2;
    if (MAGIC_EXP < 100)
        MAGIC_EXP = 100;
    mes "Nature Magic skill changed to level 2.";
    next;
    goto L_MagicSkills;

L_ChangeAstralMagicSkill:
    mes "Your level in the astral magic skill is " + @astral + ". What do you want to do?";
    menu
        "Get level 0.", L_Next9,
        "Get level 1.", L_ChangeAstralMagicSkill1,
        "Get level 2.", L_ChangeAstralMagicSkill2;

L_Next9:
    if (@menu == 1)
        updateskill SKILL_MAGIC_ASTAL, 0;
    mes "Astral Magic skill changed to level 0.";
    next;
    goto L_MagicSkills;

L_ChangeAstralMagicSkill1:
    updateskill SKILL_MAGIC_ASTRAL, 1;
    mes "Astral Magic skill changed to level 1.";
    next;
    goto L_MagicSkills;

L_ChangeAstralMagicSkill2:
    updateskill SKILL_MAGIC_ASTRAL, 2;
    if (MAGIC_EXP < 100)
        MAGIC_EXP = 100;
    mes "Astral Magic skill changed to level 2.";
    next;
    goto L_MagicSkills;

L_ChangeDarkMagicSkill:
    mes "Your level in the dark magic skill is " + @dark + ". What do you want to do?";
    menu
        "Get level 0.", L_Next10,
        "Get level 1.", L_ChangeDarkMagicSkill1,
        "Get level 2.", L_ChangeDarkMagicSkill2;

L_Next10:
    if (@menu == 1)
        updateskill SKILL_MAGIC_DARK, 0;
    mes "Dark Magic skill changed to level 0.";
    next;
    goto L_MagicSkills;

L_ChangeDarkMagicSkill1:
    updateskill SKILL_MAGIC_DARK, 1;
    mes "Dark Magic skill changed to level 1.";
    next;
    goto L_MagicSkills;

L_ChangeDarkMagicSkill2:
    updateskill SKILL_MAGIC_DARK, 2;
    if (MAGIC_EXP < 100)
        MAGIC_EXP = 100;
    mes "Dark Magic skill changed to level 2.";
    next;
    goto L_MagicSkills;

L_GetAllMagic:
    updateskill SKILL_MAGIC, 5;
    updateskill SKILL_MAGIC_LIFE, 5;
    updateskill SKILL_MAGIC_WAR, 5;
    updateskill SKILL_MAGIC_TRANSMUTE, 5;
    updateskill SKILL_MAGIC_NATURE, 5;
    updateskill SKILL_MAGIC_ASTRAL, 5;
    updateskill SKILL_MAGIC_DARK, 5;
    mes "Magic skills added.";
    next;
    goto L_MagicSkills;

L_ResetMagicSkills:
    updateskill SKILL_MAGIC, 0;
    updateskill SKILL_MAGIC_LIFE, 0;
    updateskill SKILL_MAGIC_WAR, 0;
    updateskill SKILL_MAGIC_TRANSMUTE, 0;
    updateskill SKILL_MAGIC_NATURE, 0;
    updateskill SKILL_MAGIC_ASTRAL, 0;
    updateskill SKILL_MAGIC_DARK, 0;
    mes "Magic skills removed.";
    next;
    goto L_MagicSkills;

L_AddAll:
    adddefaultskills();
    updateskill SKILL_POOL, 1;
    updateskill SKILL_MALLARDS_EYE, 9;
    updateskill SKILL_BRAWLING, 9;
    updateskill SKILL_SPEED, 9;
    updateskill SKILL_RESIST_POISON, 9;
    updateskill SKILL_ASTRAL_SOUL, 9;
    updateskill SKILL_RAGING, 9;
    updateskill SKILL_MAGIC, 5;
    updateskill SKILL_MAGIC_LIFE, 5;
    updateskill SKILL_MAGIC_WAR, 5;
    updateskill SKILL_MAGIC_TRANSMUTE, 5;
    updateskill SKILL_MAGIC_NATURE, 5;
    updateskill SKILL_MAGIC_ASTRAL, 5;
    updateskill SKILL_MAGIC_DARK, 5;

    // Real skills
    learnskill SKILL_CONFRINGO, 1;
    learnskill SKILL_ABIZIT, 1;
    learnskill SKILL_MONSTERINFO, 1;
    learnskill EVOL_AREA_PROVOKE, 1;
    learnskill SKILL_FLAR, 1;
    learnskill SKILL_CHIZA, 1;
    learnskill SKILL_MODRIPHOO, 1;
    learnskill SKILL_MODRISUMP, 1;
    learnskill SKILL_MODRIYIKAM, 1;
    learnskill SKILL_MODRILAX, 1;
    learnskill SKILL_LUM, 1;
    learnskill SKILL_PARUM, 1;
    learnskill SKILL_GOLE, 1;
    learnskill SKILL_KALAKARENK, 1;
    learnskill SKILL_KALBOO, 1;
    learnskill SKILL_KALGINA, 1;
    learnskill SKILL_KALRENK, 1;
    learnskill SKILL_HALHISS, 1;
    learnskill SKILL_HELORP, 1;
    learnskill SKILL_KAFLOSH, 1;
    learnskill SKILL_BETSANC, 1;
    learnskill SKILL_ASORM, 1;
    learnskill SKILL_INGRAV, 1;
    learnskill SKILL_UPMARMU, 1;
    learnskill SKILL_PHLEX, 1;
    learnskill SKILL_KULARZUFRILL, 1;
    learnskill SKILL_ZUKMINBIRF, 1;
    learnskill SKILL_PATMUPLOO, 1;
    learnskill SKILL_PATVILOREE, 1;
    learnskill SKILL_PATLOREE, 1;
    learnskill SKILL_MANPAHIL, 1;
    resetstatus;
    BaseLevel = 99;
    mes "All skills added to their maximum level.";
    mes "Maximum number of Legacy Magic Experience points.";
    mes "You are now level " + BaseLevel + ".";
    gmlog("Build set to max level and all skills");
    next;
    goto L_Begin;

L_ResetAll:
    //adddefaultskills();
    updateskill SKILL_POOL, 0;
    updateskill SKILL_MALLARDS_EYE, 0;
    updateskill SKILL_BRAWLING, 0;
    updateskill SKILL_SPEED, 0;
    updateskill SKILL_RESIST_POISON, 0;
    updateskill SKILL_ASTRAL_SOUL, 0;
    updateskill SKILL_RAGING, 0;
    updateskill SKILL_MAGIC, 0;
    updateskill SKILL_MAGIC_LIFE, 0;
    updateskill SKILL_MAGIC_WAR, 0;
    updateskill SKILL_MAGIC_TRANSMUTE, 0;
    updateskill SKILL_MAGIC_NATURE, 0;
    updateskill SKILL_MAGIC_ASTRAL, 0;
    updateskill SKILL_MAGIC_DARK, 0;

    // Real skills
    updateskill SKILL_CONFRINGO, 0;
    updateskill SKILL_ABIZIT, 0;
    updateskill SKILL_MONSTERINFO, 0;
    updateskill EVOL_AREA_PROVOKE, 0;
    updateskill SKILL_FLAR, 0;
    updateskill SKILL_CHIZA, 0;
    updateskill SKILL_MODRIPHOO, 0;
    updateskill SKILL_MODRISUMP, 0;
    updateskill SKILL_MODRIYIKAM, 0;
    updateskill SKILL_MODRILAX, 0;
    updateskill SKILL_LUM, 0;
    updateskill SKILL_PARUM, 0;
    updateskill SKILL_GOLE, 0;
    updateskill SKILL_KALAKARENK, 0;
    updateskill SKILL_KALBOO, 0;
    updateskill SKILL_KALGINA, 0;
    updateskill SKILL_KALRENK, 0;
    updateskill SKILL_HALHISS, 0;
    updateskill SKILL_HELORP, 0;
    updateskill SKILL_KAFLOSH, 0;
    updateskill SKILL_BETSANC, 0;
    updateskill SKILL_ASORM, 0;
    updateskill SKILL_INGRAV, 0;
    updateskill SKILL_UPMARMU, 0;
    updateskill SKILL_PHLEX, 0;
    updateskill SKILL_KULARZUFRILL, 0;
    updateskill SKILL_ZUKMINBIRF, 0;
    updateskill SKILL_PATMUPLOO, 0;
    updateskill SKILL_PATVILOREE, 0;
    updateskill SKILL_PATLOREE, 0;
    updateskill SKILL_MANPAHIL, 0;
    MAGIC_EXP = 0;
    resetstatus;
    BaseLevel = 1;
    mes "All skills removed.";
    mes "Magic experience reset.";
    mes "You are now level " + BaseLevel + ".";
    gmlog("Character reset");
    next;
    goto L_Begin;

L_close:
    closeclientdialog;
    return;
}

-	script	Debug Spell	NPC32767,{
    end;

OnDebug:
    if (!debug && getgmlevel() < CMD_DEBUG) end;
    callfunc "Debug";
    end;

OnSetVar:
    if (getarraysize(.@atcmd_parameters$) != 3)
        Exception("Usage: @set-var VARIABLE INDEX VALUE", RB_DISPBOTTOM|RB_ISFATAL);

    .@cmd$=array_shift(.@atcmd_parameters$);
    .@idx=atoi(array_shift(.@atcmd_parameters$));

    // Trying to edit global variables
    if (charat(.@atcmd_parameters$[0], 0) == "$" && !is_admin())
        Exception("You are not allowed to change global variables.", RB_DISPBOTTOM|RB_ISFATAL);

    if (charat(.@atcmd_parameters$[0],
               getstrlen(.@atcmd_parameters$[0])-1) == "$")
        .@str=true;

    if (.@str)
        .@val$=array_shift(.@atcmd_parameters$);
    else
        .@val=array_shift(.@atcmd_parameters$);

    if (.@str)
        setd(sprintf("%s[%d]", .@cmd$, .@idx), .@val$);
    else
        setd(sprintf("%s[%d]", .@cmd$, .@idx), .@val);

    .@msg$=sprintf("%s[%d] is now: %s", .@cmd$, .@idx,
                   getd(sprintf("%s[%d]", .@cmd$, .@idx)));

    if (!is_trusted())
        charcommand("@request System Information: "+.@msg$);
    else
        dispbottom(.@msg$);
    end;

// If the char is not a staff member, it'll be sent to GM Log instead
OnGetVar:
    if (getarraysize(.@atcmd_parameters$) != 2)
        Exception("Usage: @get-var VARIABLE INDEX", RB_DISPBOTTOM|RB_ISFATAL);

    .@cmd$=array_shift(.@atcmd_parameters$);
    .@idx=atoi(array_shift(.@atcmd_parameters$));

    .@mg$=sprintf("%s[%d] == %s", .@cmd$, .@idx,
                      getd(sprintf("%s[%d]", .@cmd$, .@idx)));

    if (!is_trusted())
        charcommand("@request System Information: "+.@mg$);
    else
        dispbottom(.@mg$);
    end;

OnSClear:
    sc_end SC_ALL;
    sc_end SC_DAILYSENDMAILCNT;
    dispbottom l("Status Condition Cleared");
    end;

OnAllPerms:
    if (@allperms) end;
    charcommand("@addperm all_skill");
    charcommand("@addperm all_equipment");
    charcommand("@addperm skill_unconditional");
    charcommand("@addperm join_chat");
    charcommand("@addperm hide_session");
    charcommand("@addperm any_warp");
    charcommand("@addperm view_hpmeter");
    charcommand("@addperm view_equipment");
    charcommand("@addperm receive_requests");
    charcommand("@addperm can_trade_bound");
    charcommand("@addperm bypass_nostorage");
    @allperms=true;
    end;

// These are really dangerous functions
// Force ADMs to set a variable to use, so I know they are aware of server code
OnCallFunc:
    if (!$@ALLOW_SUPERDEBUG) {
        dispbottom col("This command is restricted for senior dev-admins. Please do not use it again.", 1);
        die();
        end;
    }
    .@request$ = "";
	.@request$ += implode(.@atcmd_parameters$, " ");
    callfunc .@request$;
    end;

OnDoEvent:
    if (!$@ALLOW_SUPERDEBUG) {
        dispbottom col("This command is restricted for senior dev-admins. Please do not use it again.", 1);
        die();
        end;
    }
    .@request$ = "";
	.@request$ += implode(.@atcmd_parameters$, " ");
    doevent .@request$;
    end;

OnInit:
    registercmd "@debug", "Debug Spell::OnDebug";
    bindatcmd "getvar", "Debug Spell::OnGetVar", 40, 40, 1;
    bindatcmd "get-var", "Debug Spell::OnGetVar", 40, 40, 1;
    bindatcmd "setvar", "Debug Spell::OnSetVar", 80, 80, 1;
    bindatcmd "set-var", "Debug Spell::OnSetVar", 80, 80, 1;
    bindatcmd "sclear", "Debug Spell::OnSClear", 99, 99, 1;
    bindatcmd "allperms", "Debug Spell::OnAllPerms", 99, 100, 1;
    bindatcmd "callfunc", "@wgm::OnCallFunc", 99, 99, 1;
    bindatcmd "doevent", "@wgm::OnDoEvent", 99, 99, 1;
    // Setup debug server command overrides
    if (debug) {
        add_group_command("str", 0, true, false);
        add_group_command("agi", 0, true, false);
        add_group_command("vit", 0, true, false);
        add_group_command("int", 0, true, false);
        add_group_command("dex", 0, true, false);
        add_group_command("luk", 0, true, false);
        add_group_command("item", 0, true, false);
        add_group_command("zeny", 0, true, false);
        add_group_command("heal", 0, true, false);
        add_group_command("warp", 0, true, false);
        add_group_command("alive", 0, true, false);
        add_group_command("speed", 0, true, false);
        add_group_command("storage", 0, true, false);
        add_group_command("streset", 0, true, false);
        add_group_command("monster", 0, true, false);
        add_group_command("allstats", 0, true, false);
        add_group_command("killmonster2", 0, true, false);
        add_group_command("monsterignore", 0, true, false);

        /* TODO: DEVs need some grants as well */
        add_group_command("item", 40, true, true);
        add_group_command("zeny", 40, true, true);
        add_group_command("monster", 40, true, true);
        add_group_command("killmonster2", 40, true, true);

        add_group_command("item", 41, true, false);
        add_group_command("zeny", 41, true, false);
        add_group_command("monster", 41, true, false);
        add_group_command("killmonster2", 41, true, false);
    }
    end;
}

029-2,30,26,0	script	Debug#0	NPC154,{
    @debug_npc = 1;
    callfunc "Debug";
    end;
OnInit:
    if (!debug)
        disablenpc "Debug#0";
    end;
}

001-1,53,47,0	script	Debug#1	NPC154,{
    @debug_npc = 1;
    callfunc "Debug";
    end;
OnInit:
    if (!debug)
        disablenpc "Debug#1";
    end;
}

009-1,45,33,0	script	Debug#2	NPC154,{
    @debug_npc = 1;
    callfunc "Debug";
    end;
OnInit:
    if (!debug)
        disablenpc "Debug#2";
    end;
}

020-1,75,85,0	script	Debug#3	NPC154,{
    @debug_npc = 1;
    callfunc "Debug";
    end;
OnInit:
    if (!debug)
        disablenpc "Debug#3";
    end;
}

