// TMW2 Script
// Author: Jesusalva

// @getexp atcommand
// Gets experience (cannot be negative)
//
// group lv: 60
// group char lv: 60
// log: True

-	script	@getexp	32767,{
    end;

OnCall:
    .@delta$ = .@atcmd_parameters$[0];
    .@d = atoi(.@delta$);

    getexp .@d, 0;
    end;

/*
OnHomun:
    if (!gethominfo(0)) {
        dispbottom l("No homunculus found!");
        end;
    }
    .@delta$ = .@atcmd_parameters$[0];
    .@d = atoi(.@delta$);

    gethomunexp .@d; // [TMW2 Only] [Broken in TMW]
    end;
*/

OnGuild:
    if (getcharid(2) < 1) {
        dispbottom l("No guild found!");
        end;
    }
    .@delta$ = .@atcmd_parameters$[0];
    .@d = atoi(.@delta$);

    guildgetexp .@d;
    end;

OnInit:
    bindatcmd "getexp", "@getexp::OnCall", 60, 60, 1;
    //bindatcmd "gethexp", "@getexp::OnHomun", 60, 60, 1;
    bindatcmd "getgexp", "@getexp::OnGuild", 60, 60, 1;
}
