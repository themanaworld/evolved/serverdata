// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 046-3: Frosty Underground mobs
046-3,68,101,6,4	monster	Yeti	1072,2,100000,30000
046-3,0,0,0,0	monster	Ice Goblin	1058,20,100000,30000
046-3,55,61,12,13	monster	Moggun	1061,14,100000,30000
046-3,119,104,2,2	monster	Ice Element	1071,1,100000,30000
046-3,132,71,4,3	monster	White Slime	1093,4,100000,30000
046-3,107,85,7,4	monster	Blue Slime	1091,2,100000,30000
046-3,98,77,3,2	monster	White Slime	1093,4,100000,30000
046-3,70,82,3,2	monster	White Slime	1093,6,100000,30000
046-3,106,34,3,2	monster	White Slime	1093,4,100000,30000
046-3,33,37,3,2	monster	White Slime	1093,4,100000,30000
046-3,144,105,7,4	monster	Blue Slime	1091,2,100000,30000
046-3,152,55,7,4	monster	Blue Slime	1091,2,100000,30000
046-3,84,53,7,4	monster	Blue Slime	1091,2,100000,30000
046-3,47,23,7,5	monster	Ice Element	1071,2,100000,30000
046-3,113,48,6,4	monster	Yeti	1072,2,100000,30000
