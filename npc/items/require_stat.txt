// Unequip if stat not enough
// callfunc "RequireStat", bDex, 80, EQI_HAND_R;
function	script	RequireStat	{
    // If you can equip *anything*, this restriction is moot
    if (has_permission(PERM_USE_ALL_EQUIPMENT, getcharid(3)))
        return;
    // Load stat requisites
    .@stat = getarg(0);
    .@min = getarg(1);
    .@me = getarg(2);
    .@val = readparam(.@stat);
    // If the requirement isn't met, we set an unequip trigger.
    // The item is now removed.
    if (.@val < .@min)
        unequip(.@me);
    return;
}
