// Homunculus Database
//
// Structure of Database:
// Class,EvoClass,Name,FoodID,HungryDelay,BaseSize,EvoSize,Race,Element,bASPD,bHP,bSP,bSTR,bAGI,bVIT,bINT,bDEX,bLUK,gnHP,gxHP,gnSP,gxSP,gnSTR,gxSTR,gnAGI,gxAGI,gnVIT,gxVIT,gnINT,gxINT,gnDEX,gxDEX,gnLUK,gxLUK,enHP,exHP,enSP,exSP,enSTR,exSTR,enAGI,exAGI,enVIT,exVIT,enINT,exINT,enDEX,exDEX,enLUK,exLUK
//
// 01. Class        Homunculus ID.
// 02. EvoClass     Homunculus ID of the evolved version.
// 03. Name         Name of the homunculus.
// 04. FoodID       Item ID of the homunuclus food.
// 05. HungryDelay  Time interval in milliseconds after which the homunculus' hunger value is altered.
// 06. BaseSize     Size of the base homunculus class (0 = small, 1 = normal, 2 = large).
// 07. EvoSize      Size of the evolved homunculus class (0 = small, 1 = normal, 2 = large).
// 08. Race         Race of the homunculus (0 = formless, 1 = undead, 2 = brute, 3 = plant, 4 = insect, 5 = fish, 6 = demon, 7 = demi-human, 8 = angel, 9 = dragon).
// 09. Element      Element of the homunculus (0 = neutral, 1 = water, 2 = earth, 3 = fire, 4 = wind, 5 = poison, 6 = holy, 7 = dark, 8 = ghost, 9 = undead).
//                  The element level is always 1.
// ...
//
// Legend: b: base, gn: growth min, gx: growth max, en: evolution min, ex: evolution max
// NOTE: Only the growth values are in a 1/10 scale, the other stats are 1/1 (eg: 5 gmAGI means 0.5 agi)

// Class,EvoClass,Name,FoodID,HungryDelay,BaseSize,EvoSize,Race,Element,bASPD,bHP,bSP,bSTR,bAGI,bVIT,bINT,bDEX,bLUK,gnHP,gxHP,gnSP,gxSP,gnSTR,gxSTR,gnAGI,gxAGI,gnVIT,gxVIT,gnINT,gxINT,gnDEX,gxDEX,gnLUK,gxLUK,enHP,exHP,enSP,exSP,enSTR,exSTR,enAGI,exAGI,enVIT,exVIT,enINT,exINT,enDEX,exDEX,enLUK,exLUK
6001,6031,Mage,502,300000,2,1,7,0,700,300,45,12,20,15,35,24,14,40,55,7,9,10,20,4,15,5,14,15,25,5,15,5,15,800,2400,220,480,10,30,30,50,20,40,20,40,10,30,10,20
6002,6032,Tanker,502,300000,0,1,7,0,700,350,40,20,15,35,24,14,12,45,60,6,9,10,20,4,15,15,25,5,15,5,15,5,15,800,2400,220,480,10,30,30,50,20,40,20,40,10,30,10,20
6003,6033,Agile,502,300000,0,1,7,0,700,300,40,15,35,24,14,12,20,40,60,6,9,10,20,14,25,5,15,5,15,5,15,5,15,800,2400,220,480,10,30,30,50,20,40,20,40,10,30,10,20
6004,6034,Strong,502,300000,0,1,7,0,620,320,40,35,24,14,12,20,15,40,60,6,9,20,30,4,15,5,15,5,15,5,15,5,15,800,2400,220,480,10,30,30,50,20,40,20,40,10,30,10,20
6005,6035,Lucky,502,300000,0,1,7,0,700,320,40,24,14,12,20,15,35,40,60,6,9,10,20,4,15,5,15,5,15,5,15,15,25,800,2400,220,480,10,30,30,50,20,40,20,40,10,30,10,20
6006,6036,Accurate,502,300000,0,1,7,0,670,320,40,14,12,20,15,35,24,40,60,6,9,10,20,4,15,5,15,5,15,15,25,5,15,800,2400,220,480,10,30,30,50,20,40,20,40,10,30,10,20
6007,6037,All Rounder,502,300000,0,1,7,0,720,300,40,20,20,20,20,20,20,40,60,6,9,15,20,8,15,10,15,10,15,10,15,10,15,800,2400,220,480,10,30,30,50,20,40,20,40,10,30,10,20
6008,6038,Superior Machine,502,300000,0,1,7,0,720,300,40,15,10,30,20,30,10,40,60,6,9,10,25,4,20,5,20,5,20,5,20,5,20,800,2400,220,480,10,30,30,50,20,40,20,40,10,30,10,20

